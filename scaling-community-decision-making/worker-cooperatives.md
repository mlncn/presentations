Session Description | Descripción de la sesión

By philosophy and necessity, in a worker cooperative all people who must carry out a decision have to consent to the course of action. Everyone should get a say in the direction and conditions of their work (and no one gets to say they are just following orders).  This provides a good minimum baseline for a good decision-making process, along with hearing from everyone involved, making decisions based on data, and scheduling a time to revisit decisions.

When a decision strongly affects more than those who carry it out, however, we need better ways of making these decisions.  This is an issue for larger worker cooperatives (really anything over three people, with any degree of task specialization) as well as for hybrid/solidarity cooperatives including platform cooperatives.  Fortunately, we can scale conversations and decisions in a fair and truly democratic way.

* Come with at least a passing familiarity with various ways decisions are made in worker cooperatives, your own or others.
* Leave knowing about sociocracy and sortition and how these esoteric concepts could make our worker coops, and larger community, scale.



Who will present the session? What are the presenters’ background and experience? | ¿Quién presentará el taller? ¿Cuáles son los antecedentes y la experiencia de los presentadores? *


Benjamin Melançon (mlncn) co-founded Agaric, a worker-owned web development firm, in 2006.   He has presented at multiple technology conferences and camps on topics such as contributing to the community, authoring a (Drupal) book, and worker cooperatives.  He has also given talks at worker cooperative events and skillshares on web technology, and co-hosted a series of worker coop meetups in Boston.

Michele (Micky) Metts leads Agaric's involvement in using Free Software, like Drupal and GNU/Linux, for community building. She works to connect people with the information and tools they need to move from being a global network, to being a global movement based on solidarity. Micky advocates for the liberating power of the convergence of community building, industry organizing, free software, and cooperative development. She is a member of May First/People Link Leadership Committee, liaison between the US Solidarity Economy Network (SEN) and US Federation of Worker Cooperatives (USFWC), and a contributing author in "Ours to Hack and to Own", a handbook for the Platform Cooperativism movement which was ranked among the top tech books of 2017 by Wired magazine.



