http://votermedia.org/publications/WeWantOurCo-opsBack.pdf

Via:

Great discussion. I’ve been giving similar thought to the usefulness of Mac users groups. They used to be face-to-face meetings to both help understand and recommend products and services. I think you have hit on something re more lateral relationships. Broader governance mechanisms such as We Own It could feed this process. Mark Latham also put forward some interesting ideas in his paper We Want Our Co-ops Back http://votermedia.org/publications/WeWantOurCo-opsBack.pdf

James McRitchie
Shareholder Advocate
Corporate Governance
Site: http://www.corpgov.net 
mail: jm@corpgov.net
phone: 916-869-2402



> On Jul 30, 2019, at 12:10 PM, Nathan Schneider <n@nathanschneider.info> wrote:
>
> -discuss friends,
>
> Lately I've been floating this language of "lateral" as opposed to "vertical" relationships in co-ops—particularly the opportunity for building more member-to-member relationships and economics into legacy co-ops and credit unions, which are more focused now on conventional service provision.
>
> I just put up [a blog post on this distinction](https://cmci.colorado.edu/medlab/2019/07/29/co-ops-from-lateral-to-vertical.html ) and would love your thoughts. I also quite enjoy the accompanying photo of a sheet full of Alphonse Desjardins stamps.
>
> Hat tip to much discussion here, and especially convos with Danny Sptizberg and Taylor Nelms on member engagement.
>
> Nathan
>


News about experiments with and commentary on sortition:
https://equalitybylot.com/
