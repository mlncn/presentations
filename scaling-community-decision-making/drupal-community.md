
Any Free/Libre Open Source Software project will have elements of do-ocracy (rule of those who do the work) but this approach does not work for all decisions a software community must make.

Largely of necessity in heavily volunteer-driven projects, all people who must carry out a decision have to consent to the course of action. Everyone should get a say in the direction and conditions of their work (and no one gets to say they are just following orders).

A good decision-making process requires everyone involved be heard from, and encourages making decisions based on data and scheduling a time to revisit decisions.

We'll talk about ways we can do even better, but the nature of needing the consent of people to do the work, to carry out a decision, gives us a good minimum baseline in our processes for much of what we do.

When a decision strongly affects more than those who carry it out, however, we need better ways of making these decisions. We can scale conversations and decisions in a fair and truly democratic way.
Learning Objectives & Outcomes:

* Come with at least a passing familiarity with various ways decisions are or have been made in Drupal.
* Leave knowing about sociocracy and sortition and how these esoteric concepts could make our community scale



We need to build power to make things better.  

how we organize ourselves is *everything*

And we don't think about it enough.

And for good reason.

The average free software project has one contributor.

For most of us outside work, our coordination is thin, and attempts to formalize structures could easily kill what we have.



Saying that power is organization or coordination seems trite, let alone lacking in insight.  But given the size of many open source communities, and the power possible, there's a shocking lack of attention to the ways in which we do or might organize.

If you search for open source governance, you're likely to end up on a <a href="https://en.wikipedia.org/wiki/Open-source_governance" rel="nofollow">Wikipedia article about applying open source principles to government</a>.




Doubling down on accountability, transparency, inclusion, and participation can take you really far.

[In Search of 21st Century Democracy: Two Weeks in Taipai]https://civichall.org/civicist/two-weeks-in-taipei/






A pile of people yelling at the FindBugs maintainer
https://news.ycombinator.com/item?id=12885549





During the years in which the [open source] movement has been taking shape, a great emphasis has been placed on what are called leaderless, structureless groups as the main -- if not sole -- organizational form of the movement.  The source of this idea was a natural reaction against the over-structured [corporations and universities] in which most of us found ourselves, and the inevitable control this gave others over our lives, and the continual elitism of the Left and similar groups among those who were supposedly fighting this overstructuredness.

The idea of "[openness/structurelessness]," however, has moved from a healthy counter to those tendencies to becoming a goddess in its own right. The idea is as little examined as the term is much used, but it has become an intrinsic and unquestioned part of [open source philosophy]. For the early development of the movement this did not much matter. It early defined its main goal, and its main method, as [writing working code and] consciousness-raising, and the "structureless" [open] group was an excellent means to this end. The looseness and informality of it encouraged participation in discussion, and its often supportive atmosphere elicited personal insight.  If nothing more concrete than personal [itch-scratching and] insight ever resulted from these groups, that did not much matter, because their purpose did not really extend beyond this.




Drupal's Governance Structure

I'd call it constitutional dictatorship overlayed on a do-ocracy.





https://www.jofreeman.com/joreen/tyranny.htm

Jo Freeman (aka Joreen) is pretty awesome.

 - http://www.brooklyneagle.com/articles/2015/4/23/former-volunteers-now-brooklyn-recall-summer-voting-rights-project-1965
 - https://en.wikipedia.org/wiki/Jo_Freeman
 - https://www.jofreeman.com/index.htm



Loomio - the creators (some of them anyway, this was personal conversation and shouldn't be taken to  don't think it scales over around 100 people, though there are some groups much larger using it.




[Slide  ]
According to Ohloh, half of all open source projects only have one committer.

https://www.slideshare.net/blackducksoftware/open-source-by-the-numbers

These projects don’t have to worry about scaling community decision-making.


[Slide  ]

The weakest reason for some sort of democratic governance is still a big one:

People having the feeling of influence, of being heard.

And on that note: Drutopia has all sorts of big decisions to make, and not all that much structure!




The basic principle of justice


the logic of freedom that most kids literally work out on the school yard — you can do whatever you want as long as it

requires us to involve non-developers, something that even the leading free software projects

Free Software as a service-- "LibreSaaS" -- is an opportunity to involve non-developers—the people who are most affected by the software—dire (another reason i'm excited about Drutopia).

all this increases the need for ... scaling decision-making.  Involving more people without bogging everything down.


It's become
some very smart people— and they feel blocked


> almost everyone nowadays insists that participatory democracy, or social equality, can work in a small community or activist group, but cannot possibly ‘scale up’ to anything like a city, a region, or a nation-state. But the evidence before our eyes, if we choose to look at it, suggests the opposite. Egalitarian cities, even regional confederacies, are historically quite commonplace. Egalitarian families and households are not.

David Graeber and David Wengrow, "[How to change the coures of human history (at least, the part that's already happened)](https://www.eurozine.com/change-course-human-history/ )" (2 March 2018) accessed 2018 December.


> Term "General Assembly" gave me anxiety – and it did go the same way – whoever happened to be there… [dominated discussions and decision-making].

(An organizer in the Portland ICE building shutdown effort.  Personal notes.)



talk to someone in charge, right?




***

Inverse of Conway's Law

***

# Resources related to scaling community decision-making

ioo.coop

indieweb.org

LibreSaaS.org

drutopia.org

agaric.coop


