# Scaling Community Discussion and Decision-making

Any Free/Libre Open Source Software project will have elements of [do-ocracy](http://data.agaric.com/drupal-do-ocracy) (rule of those who do the work) but not all decisions should devolve to implementors.  Rather, a basic principle is that decisions should be made by the people who are most affected.

Particularly when a decision strongly affects more than those who carry it out, therefore, we need better ways of making decisions that give everyone their say.  This starts by letting people by heard by everyone else.  Fortunately, we can scale conversations and decisions in a fair and truly democratic way.

  * Learn why meritocracy ("rule of those with merit") is a bogus and harmful concept.
  * Gain a passing familiarity with various ways decisions are or have been made in Drupal and other projects.
  * Add sociocracy and sortition to your vocabulary and understand how these esoteric concepts can help our community scale.
  * See how [Drutopia](https://drutopia.org) is putting more democratic decision-making approaches into practice.

The most important things we have to do need to be done together.  And our work together is most powerful when we make decisions about it together.




Twitter-sized promo: How can a group of thousands of people talk about and decide anything?  How's this 'community' concept supposed to work, even in theory?


drupal community // big ideas // leadership


This session is for:
people working on developing large communities about Drupal, with Drupal, or otherwise.


Attendees will get the most out of this session by being familiar with:
communication and decisionmaking in a large community— such as Drupal


***

a space not dictated from above where everyone has a say

***

the people most affected by decisions should be those making them

communication distribution driven by a small number of people with power and money
The most important things we have to do need to be done together.  And our work together is most powerful when we make decisions about it together.

<p>Any Free/Libre Open Source Software project will have elements of <a class="reference external" href="">do-ocracy</a> (rule of those who do the work) but this approach does not work for all decisions a software community must make.</p>

<p>Largely of necessity in heavily volunteer-driven projects, all people who must carry out a decision have to consent to the course of action. Everyone should get a say in the direction and conditions of their work (and no one gets to say they are just following orders).</p>

<p>A good decision-making process requires everyone involved be <a class="reference external" href="http://www.sociocracyforall.org/projects/rounds/">heard from</a>, and encourages making decisions based on data and scheduling a time to revisit decisions.</p>

<p>We'll talk about ways we can do even better, but the nature of needing the consent of people to do the work, to carry out a decision, gives us a good minimum baseline in our processes for much of what we do.</p>

<p>When a decision strongly affects more than those who carry it out, however, we need better ways of making these decisions.&nbsp; We can scale conversations and decisions in a fair and truly democratic way.</p>




### Updated HTML version

Scaling Community Conversations and Decisions


<p>
    How can a group of thousands of people talk about and decide anything? &nbsp;How's this 'community' concept supposed to work at scale, even in theory?
</p>
<p>
    Any Free/Libre Open Source Software project will have elements of <a href="http://data.agaric.com/drupal-do-ocracy">do-ocracy</a> (rule of those who do the work), but not all decisions should devolve to implementors. &nbsp;A better ideal is that decisions should be made by the people who are most affected.
</p>
<p>
    Particularly when a decision strongly impacts more than those who carry it out, we need better ways of making decisions that give everyone their say. &nbsp;This starts by letting people by heard by everyone else. &nbsp;Fortunately, we can scale conversations and decisions in a fair and truly democratic way.
</p>
<ul>
    <li>
        Learn why meritocracy ("rule of those with merit") is a bogus and harmful concept.
    </li>
    <li>
        Gain a passing familiarity with various ways decisions are or have been made in Drupal and other projects.
    </li>
    <li>
        Add sociocracy and sortition to your vocabulary and understand how these esoteric concepts can help our community scale.
    </li>
    <li>
        Get introduced to (and give feedback on) <a href="https://visionsunite.gigalixirapp.com/">Visions Unite</a>, non-Drupal free software for mass communication mediated by the participants.
    </li>
</ul>
<p>
    The most important things we have to do need to be done together. &nbsp;And our work together is most powerful when we make decisions about it together.
</p>
<p>
    This session is for people working on developing large communities about Drupal, with Drupal, or otherwise— or existing in society.
</p>


### Twitter Teaser

The most important things we have to do need to be done together.  And our work together is most powerful when we make decisions about it together.


### Bio

<p><a href="http://agaric.com/people/benjamin-melan%C3%A7on">Bio</a></p>

<p>I have presented at multiple DrupalCons — Boston (<a href="http://boston2008.drupalcon.org/session/drupal-and-knight-foundation.html">Knight Foundation panel</a>), Washington DC (Taxonomy), Paris (Taxonomy and RDF sessions), Portland (<a href="http://portland2013.drupal.org/training/drupal-for-decision-makers.html">Decision-maker training</a>), Munich (RDF) — and led Birds-of-a-Feather at many more (on topics such as contributing to the community, authoring (<a href="https://www.goodreads.com/author/show/4222173.Benjamin_Melancon">well-regarded</a>) Drupal books, and worker cooperatives), in addition to panels, presentations, and workshops at multiple Drupal and non-Drupal summits and camps, including NYCCamp, Design for Drupal Boston, Boston GNU/Linux Meetup, New England Drupal Camp, the NERD Summit, and most recently DrupalCamp Montreal 2017 and <a href="http://2017.tcdrupal.org/session/saas-how-drupal-service-can-save-us-all">Twin Cities DrupalCamp 2017.</a></p>


### Associated resources blog post

https://agaric.coop/blog/resources-scaling-community-decision-making


### Submissions

* https://events.drupal.org/nashville2018/sessions/scaling-community-decision-making
* https://2018.tcdrupal.org/session/scaling-community-decision-making (accepted; badly presented; recording posted)
* https://www.drupaldelphia.org/session/scaling-community-decision-making
* https://2018.badcamp.net/session/scaling-community-decision-making
* **Presented:** https://drupaldelphia.org/session/scaling-community-decision-making-0
* https://www.drupalgovcon.org/2019/program/sessions/scaling-community-discussion-and-decision-making
* https://2019.badcamp.org/session/scaling-community-discussion-and-decision-making
* **Presented** DrupalCon Seattle 2019
* **Presented** New England Drupal Camp 2019
