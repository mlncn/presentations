
Version 4093 Saved October 18, 2018

Authors: mlncn
# Cooperation Planet: Libre Software and Platform Cooperatives
 
## Abstract
 
 
## Outline
 
Free Software has always been first and foremost about an ethical imperative: to respect the freedom of people using the software and the right of all people to learn from the shared knowledge embedded in software.
 
But this has not been enough, as shown both by the continued vitality of proprietary software and the prevalence of the amoral 'open source' framing in Free Software.
 
One part of this is we need to accept a new, unambiguous name, such as Libre Software, or Liberatory Software.
 
The bigger part is living up to such a new name by empracing the need to expand our ethical considerations for software to its real-world impacts.
 
"You cannot build freedom on someone else's slavery."
 
I say this a lot.  In Libre Software it means at least two very important things:
 
First, as Richard Stallman's four freedoms stipulate, you cannot build anything intended to free people on software that shackles anyone's ability to run it, study it, share it, or change it.
 
Second, we cannot write software, build organizations, or do anything at all meant to be a source of liberation—whether in tiny ways like time-freeing convenience apps or in giant ways like supporting movements for justice—without simultaneously working to rectify inequality.  At least, we can't write software or build organizations while also keeping our ethics intact unless we explicitly recognize and actively combat the myriad injustices 
 
While this sounds like adding more work onto an already-overburdened Free Software movement, it is in fact an incredible opportunity, especially in this historical moment.
 
Richard M. Stallman's famous Four Freedoms for Free Software was only three until 1990, but the formulation as "The Four Freedoms" nevertheless has, for these past several decades, echoed the even more famous Four Freedoms put forth by Franklin Deleno Roosevelt in 1941.  Formulated in the shadow of the world war wrought by the fascist powers, these four freedoms are highly instructive for the Free Software movement.
 
FDR took two more typical freedoms, "freedom of" (speech, religion), and added two more material freedoms, "freedom from"— freedom from want, from economic insecurity, and freedom from fear, from physical aggression by another nation.
 
This encapsulates a key insight from the past few hundred years of human history: people cannot in sufficient numbers work toward respecting the freedoms of others unless they are at the same time working toward better lives, even liberation, for themselves.  Indeed, many people will work against the freedoms of others in the absence of working on some genuine "freedom from"s for themselves.
 
As Audrey Eschright noted, a recession and higher unemployment decreased the volunteers in her software community.  The Great Depression of the 1930s exposed the lie which claims poverty is due to personal failings.  Similarly, temporal changes in the involvement in tech communities show why it is difficult for people who theoretically have time, but do not have stability or safe to fall back on
 
 
In rhetoric, Libre Software already puts people who use software on equal footing with people who develop software.  Freedoms zero (running) and two (sharing) are use rights, user rights, and RMS regularly explains freedom one (studying and changing) in terms of including the right of a user to hire a developer to make the changes she wants.
 
In reality, Libre Software development is dominated by developers.
 
What's wrong with developers dominating development?
 
This goes back to expanding the ethical understanding of Free Software, Libre Software, Liberatory Software.
 
If the rights to use, study and change, share, and change and share software are so essential, how much more essential is what the software does in the first place?
 
As a few people have observed, most venture-capital-backed startups set out to solve the problems of 20- and 30-something white men with money living in cities like San Francisco— and this isn't surprising given that most VC-backed startup founders are young white men with money living in cities like San Francisco.
 
The community of people building Libre Software may be more diverse than the venture-capital fueled bastion of inequality that is Silicon Valley's hot tech startup scene, but Libre Software communities are actually far less representative of the global population than the people employed to work in proprietary software.
 
This seems paradoxical, since getting into Libre Software is in some ways cost free, as opposed to having to pay for software licenses and certifications that are more common in proprietary software.  However, getting into Libre Software is a bit like being a mini-entrepreneur: you give before you get, and it carries a perception and reality of risk that is more readily borne by people with a safety net— and in the age of neoliberal shredding of government-provided social safety nets, that means people from wealthier families.  This is one filter that's keeping out people from communities with the most to gain from Libre Software.  (Another filter is related, the culture of entitlement that's in part a consequence of having a built-in bias toward people from more economically privileged backgrounds.)
 
 
We have the technology to put people, broadly speaking, in control of the software (and everything else) that affects our lives.
 
That technology is called cooperatives.
 
 
If we are going to have a broad-based movement committed to removing the freedom-denying scourge of proprietary software, we need software communities equally committed to removing the freedom-denying scourges of poverty, institutionalized racism, patriarchy (including attempts to control and punish gender-non-conforming people), government oppression, and corporate exploitation.
 
You cannot build freedom on someone else's slavery.  Our liberation, and the liberation of our software, is bound up with everyone else's liberation.
 
 
For developers as for editors, analysts, online sellers, gig economy workers, and everyone who relies in part on information technology for a living (so, yeah, pretty much everyone), making a living off of proprietary software is analogous to, and has the potential to lead to the same generations of poverty, as making a living off of someone else's land.
 
- engaging community

    - inclusivity

    - lean design methodology (i.e. gauging participation/interest early)

     

To reach its liberatory potential, the Free Software movement needs LibreSaaS and an embrace of cooperative principles.
 
 
## Reference / Prior art
 
* https://www.gnu.org/philosophy/free-sw.en.html
* Audrey Eschright, http://lifeofaudrey.com/essays/love_and_money.html
* Benjamin Mako Hill, https://media.libreplanet.org/u/libreplanet/m/free-software-and-the-shifting-landscape-of-online-cooperation/
* https://pad.drutopia.org/p/libre-present-community
* Benjamin Melançon, http://2017.tcdrupal.org/session/saas-how-drupal-service-can-save-us-all
* http://libresaas.org/
* Trebor Scholz, https://rosaluxspba.org/en/platform-cooperativism/
* "If you have come here to help me, you are wasting your time.  But if you have come because your liberation is bound up with mine, then let us work together." Lilla Watson / Aboriginal activists group, Queensland, 1970s.  https://en.wikipedia.org/wiki/Lilla_Watson
* http://blackgirlinmaine.com/racial-and-cultural/calling-all-white-people-part-28-halfway-isnt-the-way-to-justice-and-equity/
* Emily‏ @EmVVeis "Silicon Valley is solving the problems that 20-something's with money face" Should tech startups tackle bigger problems? Yes. #cyberposium    10:52 AM - 1 Nov 2014  https://twitter.com/EmVVeis/status/528605460677787648
* Making a living off someone else's land resulting in intergenerational poverty: https://qz.com/1167671/the-100-year-capitalist-experiment-that-keeps-appalachia-poor-sick-and-stuck-on-coal/ [TODO find a good article on sharecropping, which pretty directly supports the slavery metaphor]
 
### Minor fact checks
 
* On startups solving problems of wealthy, white, younger men:  Most (successful) VC-funded founders are under 35 at time of founding, https://hbr.org/2014/04/how-old-are-silicon-valleys-top-founders-heres-the-data - and most successful company founders come from wealthy backgrounds, https://www.inc.com/minda-zetlin/do-you-need-a-wealthy-background-to-be-a-successful-entrepreneur.html
 
## Potential image sources
 
* "Freedom from what?", Pops Peterson's reinterpretation of Norman Rockwell's "Freedom from Fear" for Black parents in the Black Lives Matter era, https://www.popspeterson.com/pops-at-the-roosevelt-house
 

