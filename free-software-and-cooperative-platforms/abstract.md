# Embracing the Freeing Potential of Free Software


Existing Contact: ben@agaric.coop

Session information

Session type: presentation

Session Title: Embracing the Freeing Potential of Free Software

A brief summary (This description will be printed on the Web site and in the program. Please aim for a maximum of 150 words.)

Free software, in part, begins with the premise that those who are most affected by something should have control over it.  Your software should answer to you, not to anyone else.  As a movement, we need to embrace the principle that everyone should have the most control possible over their own lives.  We must embrace the need to expand our ethical considerations for software to its real-world impacts.

There may never have been a time of more awareness of the dangers posed to us by proprietary software and unaccountable services, but we still need to meet people more than halfway.  To reach its liberatory potential, the free software movement needs to embrace cooperative principles, in development and in hosting.  This session will cover some of the ways we can do that, with specific examples like mayfirst.coop, coopcycle.org, meet.coop, and social.coop.


Abstract (This is a more elaborate explanation than the summary for the committee to understand your proposal. Please limit this to 300 words.)

Free Software has always been first and foremost an ethical imperative: the freedom of people to use software, to make it better, and to learn from the shared knowledge embedded in software.  Part of this is the premise that those who are most affected by something should have control over it.  Your software should answer to you, not to anyone else.

This is a fairly narrow view of how software can and does affect us.  This session is a detailed call to zoom out and think strategically as a movement how software can give people more control over their lives, not merely how people can have more control over our software.

As a movement, we have tended to focus on full individual freedom regarding their own software— depth-first, if you would.  Do I understand and control the program I am running, the dependencies it relies on, and hardware it runs on?  This is important and good!

We need to rethink this and in many cases go for breadth-first— covering as many people as possible, increasing freedom for as many lives as possible.  There may never have been a time of more awareness of the dangers posed to us by proprietary software and unaccountable services, but we still need to meet people more than halfway.

We need to embrace the principle that everyone should have the most control possible over their own lives.  How then, does software fit into that?  Being free in itself is only part of the way free software can be truly freeing.  What does the software do?  Is it available, accessible, usable to all who could benefit from it?

To reach its liberatory potential, the free software movement needs to embrace cooperative principles: one person one vote, member economic participation which should include users, and educating and benefiting our communities.

We also need to cooperatively host free software when hosting it is the most practical way for people to use and benefit from free software.  It is not someone else's computer if we own it collectively.  This isn't far from the early days of free software, when computing power was a shared resource.

We must embrace the freeing potential of free software and expand our ethical considerations for software to its real-world impacts.  To reach its liberatory potential, the free software movement can adopt cooperative principles, in development and in hosting.  This session will cover some of the ways we can do that, with specific examples like social.coop, coopcycle.org, mayfirst.coop, and meet.coop.


Biography (Please keep this around 100 words.)

Benjamin lives and works to connect people, ideas, and resources so we all gain the most power possible over their lives.

A web developer well-established with Drupal and PHP, he has also been enjoying programming projects with Python, with GoLang, and with JavaScript. As a co-founder of Agaric Technology Collective, a worker-owned cooperative, clients he has built for includes universities, corporations, not-for-profit organizations, and grassroots groups.  He led 34 authors in writing the 1,100 page Definitive Guide to Drupal 7.

Benjamin tries to aid struggles for justice and liberty, including as co-founder of People Who Give a Damn.


Please supply us with a photo of yourself for our Web site.  [Attached]

Please list previous presentations you've given. Feel free to include links.

A Drupal developer for 15 years, he has presented on a wide range of topics from migration and module development to design and contributing to the Drupal community. He has extensive speaking experience in the Drupal world, and has contributed sessions and trainings to many Drupal conferences.

* DrupalCon Boston (2008) - Knight Foundation panel, http://boston2008.drupalcon.org/session/drupal-and-knight-foundation.html
* DrupalCon Washington, DC (2009) – Taxonomy
* DrupalCon Paris (2009) – Taxonomy Everywhere: D7 Core Overhaul and the Expanding Contrib Universe; Paying for the Plumbing (Community, Business development and strategy)
* DrupalCon Chicago (2011) – When There's Not a Module For That: Building Drupal 7 Modules, https://chicago2011.drupal.org/sessions/when-there-s-not-module-building-drupal-7-modules.html and Better Registration / Login Workflow, https://chicago2011.drupal.org/coreconv/better-registration-login-workflow.html
* DrupalCon Denver (2012) – Building Beauty on Deadline: Project Management, Development, Multi-Device Theming, and Deployment When All Hands are On Deck
* DrupalCon Munich (2012) – RDF
* Twin Cities Drupal Camp (Minneapolis, 2019) – Shaping Drupal's Future, http://2012.tcdrupal.org/sessions/shaping-drupals-future
* DrupalCon Portland (2013) – Decision-maker training, http://portland2013.drupal.org/training/drupal-for-decision-makers.html
* NYC Drupal Summit at United Nations (2014) – Powering Your API with Drupal 8
* NERD Summit (Amherst, Massachusetts, 2015) – The future is here. What's Drupal got to do with it?, https://www.youtube.com/watch?v=G_etsXa2YNk
* IndieWebCamp New York City (2017)
* DrupalCon Nashville (2018) – Migration training, https://events.drupal.org/nashville2018/sessions/drupal-8-migrations-example
* MinneDemo (Minneapolis, Minnesota, 2019)
* DrupalCon Seattle (2019)
* Design For Drupal (Boston, 2019); Twin Cities Drupal Camp (Minneapolis, 2019) – How Drupal as a Service Can Save Our Livelihoods and our Lives, https://2019.tcdrupal.org/session/how-drupal-service-can-save-our-livelihoods-and-our-lives




In person or remote: either



Speaker release
 Yes (Creative Commons Attribution Share Alike 4.0)

This is a speaker release form, giving permission to the FSF to stream, record, and post a video of your session online. By checking one or more of the above boxes, I agree to the terms of the LibrePlanet Speaker Release.

Safe Space Policy
I have read the safe space policy and agree to follow it. 
yes


