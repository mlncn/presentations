# Drutopia: Building a Drupal SaaS Platform to Make Doing Client Work More Fun

## Speakers

* mlncn
* cedewey

Skill level: All/Intermediate

Presentation topics: business / big idea

Why would a small Drupal shop take on building a big platform?  Learn what motivates the co-founder and the newest worker-owner of a 12-years-in-business web development shop to try to steer it in a radically new direction.

The landscape is changing for web services. More and more organizations are moving to SaaS solutions such as NationBuilder, SquareSpace and Wix. Others are turning to Wordpress.  Those with larger budgets and more specialized needs still turn to Drupal to build their customized tools.  However, the gap between those who can afford and not afford a Drupal site widens.

We could keep chasing the larger budget larger sites or continue to extend our skillset into other free/libre software solutions— or we could bring into being our own large budget project, for which we'll need to extend our skillset to pull off anyway, that will allow us to support the kind of clients we like, namely, the scrappy underdogs.  If the [Drutopia](https://drutopia.org/) initiative didn't exist, we'd have to invent it, but it does, so we're joining in the effort to build a platform to provide this flexible distribution as a turnkey service.

The history of web development, as half the sessions submitted to BADCamp attest, is replacing manual work with more sophisticated software or clever hacks.  Software as a service is a significant advancement along these lines, but without the freedom of [LibreSaaS](http://libresaas.org/) it is disempowering.  Still, we don't want to spend time on work that's not specifically and uniquely valuable to the people we're working for.

Moreover, the fun parts of making a web site are uncovering the client's needs and crafting solutions.  So we're building a platform that lets us focus on those.

Follow along in our journey from worker cooperative to platform cooperative.  Debate whether scale can atone for a multitude of sins.  Grade (on a curve, please) the latest answer to the old challenge of funding free/libre software development.  Discover the excitement of building web sites in partnership with people when its power really matters to their lives.  Judge if this plan of combining small-budget customizations with a big-budget base functionality (enabled by aggregating the needs of many into one platform) is just crazy enough to work.


***


You will learn:

* Why a small Drupal shop would consider taking on building a big platform

Scale can abstract away some inefficiencies (if a thing is only done once to serve a million sites, does it matter that it's not the most efficient way to do it?) and enable a concentration of resources to reduce other inefficiencies (if a thing is inefficient on a million sites, there's a lot more justification for expending a lot of time and effort to improve it than if it's just affecting 

## What

The landscape is changing for web services. More and more organizations are moving to SaaS solutions such as NationBuilder, SquareSpace and Wix. Others are turning to Wordpress. Those with larger budgets and more specialized needs still turn to Drupal to build their customized tools. However, the gap between those who can afford and not afford a Drupal site widens.

At Agaric we can do any of the following: 

1. Focus our efforts on being more competitive in the larger budget space for custom Drupal builds
2. Support an initiative like Drutopia, which provides a similar SaaS experience, but through Drupal
3. Expand our skillset to other free software solutions.

I believe we could do any or all three. However, I believe pursuing the second best matches our expertise, market and values.

Currently our clients are nonprofits (MASS Design, Portside), health institutions (NICHQ), and education (TWiG, Patient HM). 

## Why

So far we have relied on larger budget clients in these spaces to generate revenue. This is less than ideal for a few reasons: depending too much on any single client is risky, being a smaller team scattered across different codebases brings down our efficiency, a geographically dispersed team working on separate projects breeds isolation and providing custom solutions to those with smaller budgets is challenging.

If, however, we shift our business model to supporting a single (or series of related) platforms, we deepen our expertise with a single codebase, bring higher value to smaller budget clients and foster connections between one another since we are working on the same project. Finally, it means we can better generalize and share the solutions we build with the broader free software world.

## How

Our ideal situation would be to have several clients with mid-sized budgets, all on Drutopia, funding enhancements to the platform.

These are organizations that need a more customized solution, but benefit from being part of a more generalized platform like Drutopia. When possible funded development is contributed back to the platform, but there will be times that customizations happen only on their site.

The challenge in making that transition is that we do not have any of those ideal clients right now. So, as a transitional step, I propose we support a crowdfunding initiative that will build up a reserve of funds for us to use to fund our time improving Drutopia while also serving as a marketing campaign for us to find those ideal mid-sized clients.




***

This is a companion to the following talks:

* How Drupal as a Service Can Save us All
* Scaling Community Decision-Making
* The Future is Here: What's Drupal Got to Do With It?
* Platform Cooperatives and Free Software
* Funding Free Software and Freeing Humanity^
* Seizing Power: A Community Guide^

^ doesn't yet exist as a talk or anything else
