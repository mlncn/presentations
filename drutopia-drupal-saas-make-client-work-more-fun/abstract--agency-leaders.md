# Pivoting to a Drupal SaaS Business Model to Make Client Work More Fun and Profitable

Why would a digital agency with a reputation for custom development take on building a big platform? Learn what motivates the co-founder and the newest worker-owner of a 12-years-in-business web development shop to try to steer it in a radically new direction.  It boils down to keeping the work fun and human.

The landscape is changing for web services. More and more organizations are moving to SaaS solutions such as Wix, SquareSpace, and Nationbuilder. Those with larger budgets and more specialized needs still turn to Drupal to build their customized tools. However, the gap between those who can afford and not afford a Drupal site widens.

We could keep chasing the larger budget larger sites or continue to extend our skillset into other free/libre software solutions— or we could bring into being our own large budget project, for which we're extending our skillset to pull off anyway, that will allow us to support the kind of clients we like, namely, the scrappy underdogs. If the Drutopia initiative didn't exist, we'd have to invent it, but it does, so we're joining in the effort to build a platform to provide this flexible distribution as a turnkey service.

The history of web development, as our own businesses can attest, is replacing manual work with more sophisticated software or clever hacks. Software as a service is a significant advancement along these lines, but without the freedom of LibreSaaS it is disempowering. To do right by our businesses and to do right by our clients, we need Software as a Service that's fully Free Software.

At the same time, we don't want to spend time on work that's not specifically and uniquely valuable to the people we're working for.  Moreover, the fun parts of making a web site are uncovering the client's needs and crafting solutions. So we're building a platform that lets us focus on those.

Follow along in our journey from agency to platform (and agency). Debate whether scale can atone for a multitude of sins. Discover the excitement of building web sites in partnership with people when its power really matters to their lives. Judge, from early results, if this plan of combining small-budget customizations with a big-budget base functionality (enabled by aggregating the needs of many into one platform) is just crazy enough to work.
