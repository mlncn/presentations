# Drutopia: Building a Drupal SaaS Platform to Make Doing Client Work More Fun

## Speakers

* mlncn
* cedewey

## Description

Why would a small Drupal shop take on building a big platform?  Learn what motivates the co-founder and the newest worker-owner of a 12-years-in-business web development shop to try to steer it in a radically new direction— trying to keep our work fun and human.

The landscape is changing for web services. More and more organizations are moving to SaaS solutions such as NationBuilder, SquareSpace, and Wix. Others are turning to Wordpress.  Those with larger budgets and more specialized needs still turn to Drupal to build their customized tools.  However, the gap between those who can afford and not afford a Drupal site widens.

We could keep chasing the larger budget larger sites or continue to extend our skillset into other free/libre software solutions— or we could bring into being our own large budget project, for which we'll need to extend our skillset to pull off anyway, that will allow us to support the kind of clients we like, namely, the scrappy underdogs.  If the [Drutopia](https://drutopia.org/) initiative didn't exist, we'd have to invent it, but it does, so we're joining in the effort to build a platform to provide this flexible distribution as a turnkey service.

The history of web development, as half the sessions submitted to DrupalCons and DrupalCamps attest, is replacing manual work with more sophisticated software or clever hacks.  Software as a service is a significant advancement along these lines, but without the freedom of [LibreSaaS](http://libresaas.org/) it is disempowering.

Still, we don't want to spend time on work that's not specifically and uniquely valuable to the people we're working for.  Moreover, the fun parts of making a web site are uncovering the client's needs and crafting solutions.  So we're building a platform that lets us focus on those.

Follow along in our journey from worker cooperative to platform cooperative.  Debate whether scale can atone for a multitude of sins.  Grade (on a curve, please) the latest answer to the old challenge of funding free/libre software development.  Discover the excitement of building web sites in partnership with people when its power really matters to their lives.  Judge based on early results if this plan of combining small-budget customizations with a big-budget base functionality (enabled by aggregating the needs of many into one platform) is just crazy enough to work.

## Metadata

Skill level: Advanced

Presentation topics: business / big ideas / leadership

## Experience

<p>I have presented at multiple DrupalCons — Boston (<a href="http://boston2008.drupalcon.org/session/drupal-and-knight-foundation.html">Knight Foundation panel</a>), Washington DC (Taxonomy), Paris (Taxonomy and RDF sessions), Portland (<a href="http://portland2013.drupal.org/training/drupal-for-decision-makers.html">Decision-maker training</a>), Munich (RDF) — and led Birds-of-a-Feather at many more (on topics such as contributing to the community, authoring (<a href="https://www.goodreads.com/author/show/4222173.Benjamin_Melancon">well-regarded</a>) Drupal books, and worker cooperatives), in addition to panels, presentations, and workshops at multiple Drupal summits and camps, including on earlier iterations of this topic at Twin Cities Drupal Camp, NYCCamp, Design for Drupal Boston (<a href="https://www.youtube.com/watch?v=RdJeOrEm6q4">video</a>), and the NERD Summit (<a href="http://www.youtube.com/watch?v=G_etsXa2YNk">video</a>).</p>

## Submissions

* https://2018.badcamp.net/session/drutopia-building-drupal-saas-platform-make-doing-client-work-more-fun
* https://events.drupal.org/seattle2019/sessions/building-drupal-saas-platform-make-doing-client-work-more-fun
