<?php

namespace Drupal\copyright\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block with the copyright  legend.
 *
 * @Block(
 *   id = "copyright_block",
 *   admin_label = @Translation("Copyright"),
 * )
 */
class Copyright extends BlockBase {

  /**
   * @inheritDoc
   */
  public function build() {
    return [
      '#markup' => $this->t('Copyright © @year All rights reserved', ['@year' => date('Y')]),
    ];
  }

}

