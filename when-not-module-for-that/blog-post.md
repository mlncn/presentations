# When There's Not a Module for That

We'll put the resources first, because that's what y'all came for.  But i'll blather on for a long while after them about , because it's my blog post.

## Resources

Most of these links go to a page on [Drupal's API reference which is also a great place to go for an overview](https://api.drupal.org/api/drupal/9.0.x) of what tools are available to you as you stretch and .

### Key hooks

* [hook form_alter](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21form.api.php/function/hook_form_alter/9.0.x ) 
* [hook_entity_extra_field_info](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21entity.api.php/function/hook_entity_extra_field_info/9.0.x ) + a 'field' site builders can use in entity view modes, such as to combine entity data and format it for display.  Or you can replace those three hooks with one plugin by using the [Extra field module](https://www.drupal.org/project/extra_field )

A note on all these links: I've linked to the Drupal 9.0 version.  These hooks all work in Drupal 8.x also.  Drupal.org [inadvertently for quite a while treated 8.2 as the end-all-and-be-all version of Drupal](https://www.drupal.org/project/apidrupalorg/issues/3085999 ); this has been mitigated and is being fixed but Google is still likely to take you to 8.2 when searching for documentation on a hook.  Just switch to the version you're using, or the latest version of Drupal if 
