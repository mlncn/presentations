As Dani Nordin, Dan Hakimzadeh, and I opened Chapter 4 of The Definitive Guide to Drupal:

> When building a Drupal site, “there’s a module for that” can be the sweetest words you can hear.  Modules are nicely packaged bits of code that extend what Drupal can do. With thousands of contributed modules, the odds are good that someone has written one that does something close to what you need.


> ■ Tip Finding a module that does exactly what you need in one package is most often not what you want in Drupal. Many pieces each doing their part and working together well is the way Drupal is headed. Fields and views are a prime example. You don’t want three specialized modules for a page of recipes, a list of sponsors, and a news and events section. You want Field and Views modules and friends, which you can configure to do all that and much more. (Features modules, and other modules that define their own views and create content types for you, try to provide the best of both worlds by packaging up this configuration work to have drop-in functionality, but you can modify or extend it if you need to using flexible, widely used tools.)
