# Migrate Your Way to Drupal 8 Greatness


See raw notes for updated description as submitted to NEDcamp 2024.


Don't let getting the content over stop you from moving to Drupal 8— whether from another version of Drupal or some other something else entirely.

Don't let moving the content over be an afterthought, either.

We will:

     Introduce you to Drupal's Migrate API, which is the powerful Migrate module moved into core for Drupal 8.
     Show you how to upgrade your Drupal site with Migrate API, which is now the official upgrade path for Drupal 6 and Drupal 7 sites upgrading to Drupal 8.
     Provide strategies for approaching migrating content from different sources into Drupal.
     Detail the steps for migrating from some common non-Drupal sources.
     Answer your questions to the best of our ability! Feel free to ask them in the comments below or ask us directly, and we'll try to incorporate it into the session.

Learning Objectives & Outcomes:

Have confidence in your ability to use Drupal 8's core Migrate module to bring in content from Drupal datases, other databases, and flat files.

We might even throw in a little Python-powered preparation of decade-old HTML if you're all of hardy constitutions, but mostly we'll be covering how Drupal 8 gives you the tools to get content into your beautifully constructed Drupal 8 site.


## Metadata

Experience level: Advanced

Drupal version: Drupal 8 (current)

## Speaker credentials

<p>I have presented at multiple DrupalCons — Boston (<a href="http://boston2008.drupalcon.org/session/drupal-and-knight-foundation.html">Knight Foundation panel</a>), Washington DC (Taxonomy), Paris (Taxonomy and RDF sessions), Portland (<a href="http://portland2013.drupal.org/training/drupal-for-decision-makers.html">Decision-maker training</a>), Munich (RDF) — and led Birds-of-a-Feather at many more (on topics such as contributing to the community, authoring (<a href="https://www.goodreads.com/author/show/4222173.Benjamin_Melancon">well-regarded</a>) Drupal books, and worker cooperatives), in addition to panels, presentations, and workshops at multiple Drupal, other tech, and non-tech summits and camps.</p>

<p>I co-presented an <a href="https://events.drupal.org/nashville2018/sessions/drupal-8-migrations-example">beginner/intermediate version of this training lab most recently at DrupalCon Nashville</a>.</p>


## Submissions

* http://2016.tcdrupal.org/file/migrate-your-way-drupal-8-greatness (presented...  TERRIBLY)
* https://events.drupal.org/seattle2019/sessions/migrate-your-way-drupal-8-greatness
