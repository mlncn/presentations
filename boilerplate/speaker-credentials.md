# Speaker credentials (for organizers only)

"Tell us about your experience as a speaker"


## Migration training

<p>I have presented at multiple DrupalCons — Boston (<a href="http://boston2008.drupalcon.org/session/drupal-and-knight-foundation.html">Knight Foundation panel</a>), Washington DC (Taxonomy), Paris (Taxonomy and RDF sessions), Portland (<a href="http://portland2013.drupal.org/training/drupal-for-decision-makers.html">Decision-maker training</a>), Munich (RDF) — and led Birds-of-a-Feather at many more (on topics such as contributing to the community, authoring (<a href="https://www.goodreads.com/author/show/4222173.Benjamin_Melancon">well-regarded</a>) Drupal books, and worker cooperatives), in addition to panels, presentations, and workshops at multiple Drupal summits and camps.</p>

<p>I co-presented an <a href="https://events.drupal.org/nashville2018/sessions/drupal-8-migrations-example">earlier iteration of this training lab most recently at DrupalCon Nashville</a>.</p>


un-HTMLd:

I have presented at multiple DrupalCons — Boston ( http://boston2008.drupalcon.org/session/drupal-and-knight-foundation.html - Knight Foundation panel ), Washington DC (Taxonomy), Paris (Taxonomy and RDF sessions), Portland ( http://portland2013.drupal.org/training/drupal-for-decision-makers.html - Decision-maker training ), Munich (RDF) — and led Birds-of-a-Feather at many more (on topics such as contributing to the community, authoring ( https://www.goodreads.com/author/show/4222173.Benjamin_Melancon - well-regarded) Drupal books, and worker cooperatives), in addition to panels, presentations, and workshops at multiple Drupal and non-Drupal summits and camps, including NYCCamp, Design for Drupal Boston, Boston GNU/Linux Meetup, New England Drupal Camp, the NERD Summit, and most recently DrupalCamp Montreal 2017 and - http://2017.tcdrupal.org/session/saas-how-drupal-service-can-save-us-all - Twin Cities DrupalCamp 2017.


Future is here
https://www.youtube.com/watch?v=RdJeOrEm6q4




## Abbreviated Bio (500 characters)

Co-founder Agaric Technology Collective, a web development and strategy consultancy which helps people create and use powerful Internet technology.  Founding elected director of the Amazing Things Arts Center in Framingham, Massachusetts. In 2010-2011, led 34 authors in writing the 1,100 page Definitive Guide to Drupal 7. More recently, he has been helping build the Drutopia platform. Benjamin uses libre software to give people and groups control over their online communication and web presence.



Older:

<p>I have presented at multiple DrupalCons — Boston (<a href="http://boston2008.drupalcon.org/session/drupal-and-knight-foundation.html">Knight Foundation panel</a>), Washington DC (Taxonomy), Paris (Taxonomy and RDF sessions), Portland (<a href="http://portland2013.drupal.org/training/drupal-for-decision-makers.html">Decision-maker training</a>), Munich (RDF) — and led Birds-of-a-Feather at many more (on topics such as contributing to the community, authoring (<a href="https://www.goodreads.com/author/show/4222173.Benjamin_Melancon">well-regarded</a>) Drupal books, and worker cooperatives), in addition to panels, presentations, and workshops at multiple Drupal summits and camps, including on earlier iterations of this topic at NYCCamp, Design for Drupal Boston, and the NERD Summit.</p>

Some of my other Drupal contributions can be <a href="https://www.drupal.org/u/mlncn">seen on my d.o profile</a>.

***

<p>This presentation will build on iterations of Shaping Drupal and Future is Here talks given over the past four years at camps and summits, including <a href="http://2012.tcdrupal.org/sessions/shaping-drupals-future">Shaping Drupal's Future</a> at Twin Cities Drupal Camp, and Future is Here at NYCcamp, <a href="http://design4drupal.org/design-4-drupal-2015/sessions/future-here-whats-drupal-got-do-it">Design 4 Drupal Boston 2015</a> (<a href="https://www.youtube.com/watch?v=RdJeOrEm6q4">video</a>) and <a href="https://nerdsummit.org/nerdsummit-2015/sessions/future-here-whats-drupal-got-do-it">Nerd Summit 2015</a> (<a href="http://www.youtube.com/watch?v=G_etsXa2YNk">video</a>).</p>

Ben is passionate about using technology to connect people and to help them gain control over their lives and projects. A Drupal developer for more than 10 years, he has presented on a wide range of topics from migration and module development to design and contributing to the Drupal community. He has extensive speaking experience in the Drupal world, and has contributed sessions and trainings to several DrupalCons, including:

### Presenter

* DrupalCon Boston (2008)
* DrupalCon Washington, DC (2009)
* DrupalCon Paris (2009)
* DrupalCon Chicago (2011)
* DrupalCon Denver (2012)
* DrupalCon Portland (2013)
* MinneDemo (Minneapolis, Minnesota, 2018)
* DrupalCon Seattle (2019)

### Trainer

* DrupalCon Portland

[[See if there are any additional drupalcon and regional event sessions where Ben co-presented with Mauricio or Todd]]

Ben has also presented at many regional events, including:
BADCamp
Design 4 Drupal
Drupal Corn
DrupalCamp NJ
NERD Summit
New England Drupal Camp
NYCamp
Pacific Northwest Drupal Summit
Twin Cities Drupal Camp

Ben has also been the featured speaker at various local meetups, including:
Drupal Nights (Cambridge, MA)
Boston Drupal Meetup
New York City Drupal Meetup
Providence Drupal Meetup

Featured guest:
Drupal Easy podcast



Drupal Nights (don't link to this) Powering Your API with Drupal 8 https://www.youtube.com/watch?reload=9&v=PILhh5kGv0w
