Philosophy:

* Listen
* Learn
* Loop

Main challenges:

* Listening to the client more than the citizenry

Make sure to cover:
* Define iterative UX. Brief description of iterative UX/iterative testing/validation, and how this approach is ideal for any project, particularly this project
* Describe the research that Cambridge had done before engaging Agaric, and explain how/why that helped so much
    * Clients and Leo conducted research on sites with similar functionality "took great notes on what worked well and what didn't" -- did this include interviewing owners of the similar sites and/or cambridge clients' personal assessments of what worked well?
        * Cambridge had some graduate students conduct interviews with other website owners -- a whole semester project
    * Domain research: they found that people expected more useful information from .org domains than from .gov domains
    * Comparative analysis: Cambridge staff evaluated similar sites and came up with critiques and questions about look/feel/functionality
        * Discovered that site needs to be mobile friendly

Findings
* Automatic translation doesn't work well - give example of Google Translate button vs automatic machine pre-translation
    * 

For iteration section of presentation:
* when discussing iterating on what someone else has built, focus on what works well and what you want to change


What's next:

* Make continued investment in iterative UX more affordable (maybe) by building a platform


Examples:


* Power users reported not finding organizations with terms they expected, specifically, when searching for words that were in the title or description of programs that the organization provided, they expected to see the organization (even though no one had bothered to put such presumably crucial words in the description of the organization itself).

## See also

* Notes for improving: https://pad.drutopia.org/p/find-it-presentation-notes
* Case study written since presentation: https://agaric.coop/work/find-it-cambridge
* More info but less vetted than the case study on Agaric.coop above: https://docs.google.com/document/d/1yPQ1te-xdPlLzyHqaqxckIuBm2I_0YGA0l4kuaXtKMg/edit
* Original presentation slides: https://docs.google.com/presentation/d/1NGQrIx0Oft0Z6MzJ3MBhPkQyAdZrn6CVdegkh7trE1Q/edit
* Newer 30 minute version: https://docs.google.com/presentation/d/1RLYlD03gGwIoLRpFDF1XQaciMVu4OYMXbLmT3FUObFA/edit
* Find It platform for using approach and tech with other cities: https://gitlab.com/agaric/find-it
