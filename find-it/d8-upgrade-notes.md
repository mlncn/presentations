Existing site gets a fifth of its traffic direct— a little bit more, 22%, and that's actually quite high.

As our frightningly logical German colleague puts it, does it make any sense to invest in improving internal search when everyone is getting there via Google or another search engine anyway?
