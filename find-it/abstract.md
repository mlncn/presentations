# Iterative UX: Find It Cambridge Case Study

Improving the experience for interacting with an entire city.

A handful of visionary people in the city of Cambridge, Massachusetts wanted an online resource to help families easily find activities, services, and resources.

Find It Cambridge was informed and further inspired by extensive initial research done by Code for Boston and the City of Cambridge.  As this was wrapping up, the city brought Agaric in, so we found ourselves in the incredibly fortunate situation of starting with this in-depth knowledge.

This session will start with highlights from this research conducted before anything was built.  Next, we will take you through the iterations of design and mockup testing led by Todd Linkner, our design partner.  We'll also cover the learning from the initial user testing with people in a library computer lab, which was particularly real-world conditions for this site!  Finally, we will go into the iterative improvements continuing through today, regularly informed by feedback from people using the site in real conditions.

We will also discuss the challenge of balancing client feedback with user feedback and offer strategies for making sure user needs are prioritized, including how to be a user advocate no matter what your role in a project is. We will take you through some of the client-developer decision processes and present what we've learned and how we plan to continue building on what we've learned as we continue to build on this amazing citizen resource.

The philosophy throughout has been listen, watch, learn, and then use the knowledge gained to develop and prioritize enhancements and new features. The most interesting parts, as so often, lie in the details.  This session will also present frankly what could have been done better, and what could still be done better.



## Twitter-sized promo

Keep improving the experience of people visiting your client's site— and keep improving your and your client's experience working on and with the site!


## Speaker credentials

<p>An <a href="https://www.youtube.com/watch?v=XjRO2OTt-5o">early version of this talk</a> was given, with the clients in attendance, <a href="https://design4drupal.org/sessions/case-studypanel/iterative-ux-find-it-cambridge-case-study">at Design4Drupal Boston 2018</a>.</p>

<p>I have presented at multiple DrupalCons — Boston (<a href="http://boston2008.drupalcon.org/session/drupal-and-knight-foundation.html">Knight Foundation panel</a>), Washington DC (Taxonomy), Paris (Taxonomy and RDF sessions), Portland (<a href="http://portland2013.drupal.org/training/drupal-for-decision-makers.html"> Decision-maker training</a>), Munich (RDF) — and led Birds-of-a-Feather at many more (on topics such as contributing to the community, authoring (<a href="https://www.goodreads.com/author/show/4222173.Benjamin_Melancon">well-regarded</a>) Drupal books, and worker cooperatives), in addition to panels, presentations, and workshops at multiple Drupal summits and camps, including Drupal Camp Twin Cities, NYCCamp, Design for Drupal Boston, and the NERD Summit.</p>

<p>Some of my other Drupal contributions can be <a href="https://www.drupal.org/u/mlncn">seen on my d.o profile</a>.</p>


## Submissions

* **PRESENTED** https://design4drupal.org/sessions/case-studypanel/iterative-ux-find-it-cambridge-case-study 
* https://events.drupal.org/seattle2019/sessions/iterative-ux-find-it-cambridge-case-study
* **PRESENTED** https://drupaldelphia.org/session/iterative-ux-find-it-cambridge-case-study
* https://www.drupalgovcon.org/2019/program/sessions/iterative-ux-find-it-cambridge-case-study
* https://2019.badcamp.org/session/iterative-ux-find-it-cambridge-case-study
* https://nedcamp.org/sessions/2019/iterative-ux-find-it-cambridge-case-study


## Outline

 * Recurring dates patterns — a service provider / editor "quality of life" improvement
   - When we initially implemented this, the choices for an opportunity with more than one occurrence were RRULE based dates *or* real in-the-database repeated dates
   - We bring events in from Cambridge Public Libraries which has many 
   - I had to keep stopping myself from trying to write a conversion from a random assortment of dates to an RRULE
   - In Other Words retrofitted to use Smart Date
 * Minimal requirements to save, but more requirements to publish— Require on Publish module
 * Feedback from users (?) about seeing Young Adult, Adult, and Senior Adult was confusing just in its wordiness
   - Enhanced In Other Words for this, choosing an arbitrary number of terms and if at least X are selected, print a replacement instead.
   - Could for instance choose 13 through 19 and if all of those are selected print "Teenagers"
 * COVID-19!
   - banner block
   - virtual or in person

### Big lessons

  * Stay listening
    - "Active listening", to a whole city or potential users of a website, means going out and finding people.
    - Give people a way to talk to you
  * That segues into "something is better than nothing", the suggest updates link just opens in an e-mail
