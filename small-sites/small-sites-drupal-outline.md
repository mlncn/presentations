
This is a followup to January's meeting, which unfortunately i was out of town for, but i understand we learned about Drupal for big projects.

Here we ask the followup question:  Is there still a place for small sites in the Drupal ecosystem?

Yes. The same features that make Drupal projects consistent, repeatable, and maintainable for large corporations can contribute to an affordable and independent offering for much smaller websites.

Drutopia is an ecosystem of Drupal distributions that is trying, in various ways, to bring Drupal's enterprise capabilities to everyone.  The project is at a crossroads of the two ways to make the power of Drupal accessible to grassroots groups— solely as a quick-start distribution, or as a cooperative platform that offers an alternative to services like Squarespace or Wix without the black-box, proprietary code business model and vendor lock-in.

Initial description from https://www.meetup.com/Twin-Cities-Drupal-Group/events/267069414/


First question of course is why?  We're cold, calculating professionals, every one of us.  We use the best tool available.

Why not WordPress or Backdrop or Ghost or Grav or Jekyll or Hugo?  Why not Ruby on Rails or Django?  Why not SquareSpace or Wix, for that matter?

Here are some reasons you may want to use Drupal:

  * Webforms
  * Structured content
  * Faceted search
  * Fine-grained permissions
  * Development workflow that's not just editing configuration on live
  * The possibility of building most any functionality into the site
  * The ability to both read and write to APIs, cognizant of your data model
  * It's in your control: Libre software and you can choose where to host
  * Use what you know

What are your reasons?

With the why rationalized, let's move on to how.


Approaches:

  * Make things easier
  * Do things at a sufficient scale that the cost of the difficult parts are spread thinly across many sites



## Making things easier

  * Sharing configuration (feature modules)
  * Good documentation
  * Just making the software easier to use (including modules to replace custom code, like my "In Other Words" module)
  * [Skins](https://www.drupal.org/project/skins ) instead of subthemes, an approach Drutopia is now taking.
  * Most of what you need already built— distributions.


### Distributions

What kind of site are you trying to build?  There may be something that gives you a start.

  * Restaurant site, with [Open Restaurant](https://www.drupal.org/project/openrestaurant)
  * Small business site, with [Druppio](https://www.drupal.org/project/druppio_small_business_distribution )
  * Online magazine or newspaper, with [Thunder](https://www.drupal.org/project/thunder )
  * Online community site allowing active subgroups, with [Open Social](https://www.drupal.org/project/social )
  * Program and event locator, such as for finding out-of-school-time opportunities for kids, with [Find It](https://agaric.coop/findit )
  * Church site, with [OpenChurch](https://www.drupal.org/project/openchurch )
  * Corporate site, with [Droopler](https://www.drupal.org/project/droopler )
  * Learning management system, with [Opigno LMS](https://www.drupal.org/project/opigno_lms )
  * Research and grant management, with [Niobi](https://www.drupal.org/project/niobi )
  * Government sites, [GovCMS](https://www.drupal.org/project/govcms8 )
  * Volunteer coordination, with [Volunteer Center](https://www.drupal.org/project/vc )
  * and [more](https://www.drupal.org/project/project_distribution?f%5B0%5D=&f%5B1%5D=&f%5B2%5D=drupal_core%3A7234&f%5B3%5D=sm_field_project_type%3Afull&f%5B4%5D=&f%5B5%5D=&text=&solrsort=ds_project_latest_activity+desc&op=Search )
  * including [Varbase](https://www.drupal.org/project/varbase ), a sort of generic starting point.
  * and of course, for a grassroots group, [Drutopia](https://www.drupal.org/project/drutopia ).

You can take any one of these distributions, and build your site on it.  It can give you a big head start.  It may bring some headaches, too, but, that's Drupal.  That's web development.  That's life.  Still, grab and hack, which is a reason to start with a product-and-a-framework in one, a content management system and a framework in one like Drupal, in the first place, can give your small site a boost.

If you change much, however, you've likely essentially forked the distribution, and have a Drupal site that was once based on Distribution X.  And that's fine!  

### Drutopia's approach to distributions

#### Swappable pieces

With Drutopia we also tried to go a bit further to make it a flexible distribution that can be built on.

We didn't go as far as i would have liked, which would be to make the idea of a base distribution obsolete.  I wanted to explode every requirement into its tiniest component part:  Does a feature need a given role?  It depends on a feature module that provides that role, and nothing else.  An image style?  Same thing, a feature module providing just that image style.

Reasonably rejected as impractical, we still went to a lot of work to be able to enable, disable, and potentially swap out various features, including:

* [People](https://gitlab.com/drutopia/drutopia_people ) content type and listing page
* [Campaigns](https://gitlab.com/drutopia/drutopia_campaign )
* Actions
* Events
* Articles
* Blogs
* Comments
* Landing pages
* Resources
* Faceted search
* Collections
* Storylines
* Related content
* Webforms
* Groups
* SEO tools

But not many heeded our call, and distributions are essentially incompatible with each other.

#### Base theme

To make all this functionality and configuration easy to work with out of the box, we provided, Octavia, a theme based on the flexible, flexbox-based Bulma CSS framework.

#### Upgradeable

The other thing we (read: Nedjo Rogers) put a lot of time into is trying to make it so that when 


#### Development environment

All the tools you need to work like a huge agency in a tiny little package:

  * Database
  * PHP
  * Composer
  * Sass

Even deployment tools, if that's not handled another way.


All of these are big dreams, that to do well, requires resources.  Which is why part of our dream is to get big.


## Growing big enough that the bumps seem tiny


And one way to get big, and *also* to immediately benefit smaller sites, is to provide websites as a service.

### SaaS

Software as a Service is of course a huge business.  You probably use accounting software, timetracking or invoice software, or something else that runs on someone else's computer and you actually pay for.  And then there's the social media (Facebook and Twitter), office suites (Google Docs and Google Calendar), and the like that we might not pay for with dollars, but we're giving our data.

More in the website space:

  * Wix, Squarespace, Weebly, etc.
  * Salesforce

And we have Software as a Service in the Drupal world, too:

  * Pantheon, almost devops as a service
  * Acquia, not so much on the hosting tools but with proprietary Drupal enhancements
  * Localeyz, a Drupal-based service for local media, but their code is not shared as far as i can tell.


### Distributions + SaaS == LibreSaaS

If you maintain a Drupal distribution, ready for anyone to use

And you provide hosting that people can pay for, subsidizing the cost of developing that distribution

That's LibreSaaS — wholly free libre software available as a service.


(A term I coined, but someone else coined it independently.  Which makes it a movement.)

LibreSaaS is a business model that works for Free/Libre Open Source Software (even if people don't *care* whether it is free/libre or not, at least libre software isn't at a disadvantage.

LibreSaaS has qualities that are enterprise and movement friendly (the easy to get started, can take full ownership/control as needed angle)

  * Round Earth, Drupal + CiviCRM by Drupal support and maintenance experts MyDropWizard.
  * Open Social
  * [Open Church](http://openchurchsite.com/ )
  * [Farmier](https://farmier.com ), in Drupal 7.

Then, not a Drupal distribution but Drupal adjacent, there's Centarro:

Commerce Guys rebranded as Centarro as they move to offering software as a service— taking on the especially tricky bits of commerce that you don’t want running on your site.

Ryan Szrama encapsulated LibreSaaS beautifully when he said that in addition to the “no lockin” part of being fully GPL Libre Software, the promise of greater collaboration is why they didn’t go proprietary SaaS.

And also Roomify, built on Drupal 8.

Probo.CI, not Drupal but built by ZivTech, a longtime Drupal company.

Outside Drupal, there's a lot more examples, and a lot more economically thriving platforms that charge for their hosting services but make their code freely available:

  * Discourse
  * Ghost
  * Tendenci
  * Wallabag, a pocket-like service
  * [Goteo](https://en.goteo.org/ )
  * [Open Collective](https://opencollective.com/ )
  * Standard Notes
  * Write.as (formerly Overleaf)
  * [Loomio](https://loomio.org )
  * WordPress.org

See more at LibreSaas.org

That's a lot, really.  But why not more?  And why are few people, even in tech, even aware of it as an idea and a practice?

Every cloud service, every SaaS, all the big platforms you can think of are almost invariably built on a mostly Free Software stack: the operating system, the server, the programming languages, the databases, the libraries, all of it!

Some, like Slack or most infamously AWS, are making piles of money on a relatively thin slice of proprietary code pulling it all together.  Why can’t FLOSS move into that?

I'm giving this talk because I think part of the reason is lack of awareness as a business model.  And therefore, lack of explaining its benefits.



# SaaS + Democracy == Platform Cooperative

A cooperative is a jointly owned and democratically-controlled enterprise formed by people voluntarily uniting to meet their common needs and aspirations. https://agaric.coop/blog/putting-powerful-platforms-under-cooperative-control

Localeyz, the closed-source Drupal-as-a-service mentioned earlier, is also cooperative platform.

See many more at https://ioo.coop/directory/


The software cloud, infamously, is just someone else's computer.

But it's *not* someone else's computer when you own it collectively with everyone else using the service.





# Redecentralization

I've essentially been talking about centralizing our way to being able to compete with the proprietary platforms.

That hasn't been fashionable in tech circles since... ever.  We're supposed to create decentralized protocols 

and of course, [Mastodon](https://github.com/tootsuite/mastodon#readme ) is a pretty big success (find me on social.coop), but despite the relative decentralization, it's a fairly fragile ecosystem.

Mastodon went public four years ago today


Completely decentralized approaches, like Scuttlebutt, are not hitting that ease of use

Cooperative platforms can provide the business model which 




# Cooperative Platforms powered by LibreSaaS


  * [Social.coop](https://wiki.social.coop/home.html ) and [Sunbeam.City](https://sunbeam.city/about/more ), running on Mastodon (and also using Loomio and Open Collective).
  * [CoopCycle](https://coopcycle.org/en/ ), the European federation of bike delivery coops. Governed democratically by coops, it enables them to stand united and to reduce their costs— including by producing Libre Software.
  * [Fairmondo](https://www.fairmondo.de/ ), an online marketplace (Germany).
  * [May First Movement Technology](https://mayfirst.coop/en/ )
  * [Webarchitects Co-operative](https://www.webarchitects.coop/ ), a multi-stakeholder co-operative providing ethical (including Free/Libre Open Source Software) and green, web hosting, virtual servers, and GNU/Linux sysadmin support services
  * [RChain Cooperative](https://rchain.coop/ ), a blockchain platform of social coordination technologies.


# Community

At the end of the decade, it's community that matters.

Benjamin Mako Hill: A decade ago, the kind of mass collaboration that made Wikipedia, GNU/Linux, or Couchsurfing possible was the exclusive domain of people producing freely and openly in commons. Not only is this no longer true, new proprietary, firm-controlled, and money-based models are increasingly replacing, displacing, outcompeting, and potentially reducing what's available in the commons.

For example, Salesforce has built a hugely impressive community— there will be a few more people than we have here going to their meetup in Minneapolis tomorrow, and every Saturday.

It's not all bad.  If you search for "Squarespace" or "Wix" on Meetup.com, the only meetup that comes up is this one!

Of course, Meetup.com itself sold out to the biggest real estate scam of .. well, the past two years .. [WeWork](https://www.currentaffairs.org/2020/01/wework-is-a-scam ).

Meetup was the epitome of the sustainable growth startup, yet they got pulled into the WeWork scam.

Late last year, undoubtedly under the influence of their new, bankrupt bosses, Meetup tried to put out a $2 per attendee charge, and took down the blog post even mentioning after general rebellion.

Real shame they aren't open source, with full data portability, for when the venture capitalists in charge inevitably start siphoning our blood to feed their investors.

... and there's [work to get events and meetups capabilities off the ground in the ActivityPub space](https://git.feneas.org/feneas/fediverse/-/wikis/watchlist-for-activitypub-apps#events-and-meetups

but discoverability is hard.

So again: More central platforms, under control of the people who use them, are a real, valid solution for where we find ourselves in the year of hindsight, 2020.



But four years since Drutopia launched, in part to save Drupal from itself, we have to acknowledge that people have spoken.  Most development shops sticking with LibreSaaS choose WordPress— that's pretty much always been the case, but the gap is growing, mostly because Drupal isn't growing at all.


[Backdrop](https://backdropcms.org ) has a small but very healthy community and is the clear place to go for people who like "Drupal Classic".


I want to give it one more try.  Drupal 8 and 9 have a lot to offer.  Drupal still has a cool community.

We can cooperate to build what we need, something the world needs.


Historically, under pressure, many people have turned to forming cooperative associations.

Jessica Gordon Nembhard chronicles many of these in the African-American community throughout the history of the United States.

The Federation of Southern Cooperatives, started in 1967 to help shore up an economic foundation to civil rights, 


Wisdom from the labor movement, spoken at a conference for cooperatives:

"If we're not focused on things that build scale, we're not building institutions that change society.  And if we're not building institutions that change society, we're not doing what we need to do."
     — David Hammer (ICA)


# Democratic, cooperative communication

It's no secret, as Frank Chapman of the [National Alliance Against Racist and Political Repression](https://naarpr.org/ ) reminded people just a few blocks from here, that you build political power by going door to door and finding supporters.

What would our political movements be able to do if we didn't have to redo all the grunt work every time?

Or if people weren't canvassed only by campaigns, but asked about their needs?

In this area, we'd probably have gotten all the lead out of the drinking and cooking water by now.

We might have a solution to the problems of people being unable to afford to live here.



Decline of groups.drupal.org, drupal.org

Meetup
Stack Exchange
Slack



Do you maintain a site or have a client with a lot of community interaction on it?

Groups, forums, posts with comments?

We at Agaric have an example of each: NICHQ, TeachersWithGUTS.org, GEO.coop



Drutopia?

Failed to be community-first.  Not sure that's possible but that's what i'd like to aim for now.



join the actually-existing federated social web

That's what [Mastodon did](https://blog.joinmastodon.org/2018/06/why-activitypub-is-the-future/ )


Community

new adopters can get started, knowing they don't have the dead-end or Wile E. Coyote situation of the ground being pulled out from under them

convenient
no lock-in



# Transparency does not equal democracy

The concentrated benefit, diffuse cost conundrum helps explain a mystery uncovered by James D'Angelo: that the move to public votes in the US House of Representatives in 1970 increased the power of big donors, lobbyists, and political parties.  Before, when most votes were secret, representatives had the option of lying to their donors, to lobbyists, to party "whips", and thus to vote their conscience.





Images:

> ["Circuit-Bending with the Easy Button"](https://www.flickr.com/photos/35237092540@N01/5462062551) by [Pete Prodoehl](https://www.flickr.com/photos/35237092540@N01) is licensed under [CC BY-NC-SA 2.0](https://creativecommons.org/licenses/by-nc-sa/2.0/?ref=ccsearch&atype=rich)

(uncredited?) found at https://www.britannica.com/explore/savingearth/mass-production/




***

Imagine if Pantheon gave 10% of the revenue made from every site to the people developing the underlying software for each site.


Drupal Association, by it's constitution, doesn't work on general need code.

Is Pantheon supposed to give money to Automattic?  (OK in responding to questions i think i forgot that there does exist a separate WordPress.org foundation or something.  But does it employ any developers?)



***

Resources:

* https://agaric.coop/blog/putting-powerful-platforms-under-cooperative-control
* https://www.grocer.coop/articles/bylaws-how-strong-your-co-ops-foundation (article from 1988, posted on a Drupal site!)
* 
