
In January's meeting, we learned about Drupal for big projects. Is there still a place for small sites in the Drupal ecosystem?

Yes. The same features that make Drupal projects consistent, repeatable and maintainable for large corporations can contribute to an affordable and independent offering for much smaller websites.

Benjamin Melançon from Agaric will tell us all about Drutopia, an ecosystem of Drupal distributions. The project is at a crossroads of the two ways to make the power of Drupal accessible to grassroots groups— solely as a quick-start distribution, or as a cooperative platform that offers an alternative to services like Squarespace or Wix without the black-box, proprietary code business model and vendor lock-in.

Initial description from https://www.meetup.com/Twin-Cities-Drupal-Group/events/267069414/
