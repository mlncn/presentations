# How Drupal as a Service Can Save Our Livelihoods and our Lives

Drupal is better than ever, but whether it is more successful is questionable. A pincer threatens Drupal. One side, Drupal's own power and complexity, discourages new users and contributors. The other, proprietary platforms, increasingly squeeze out custom web development through sheer economies of scale. Retreating into Drupal's new fortress, the enterprise, leaves many of us on the outside— and it doesn't escape the pincer, which will continue until there's nowhere left to hide.

Adding a new economic model, that of software as a service, can make Drupal the best choice for quick starts and for nimble organizations. This, in turn, can drive contributions and innovation. Instead of waiting for proprietary SaaS companies to slowly add features and come eat our lunch, we can swoop in and eat theirs. At the same time, a well-designed Drupal SaaS provides many more people with the traditional entryway to web development and to Drupal (namely, hacking around with HTML and clicking together functionality).

Adhering to the principles of Free/Libre Software with our platforms will make the Drupal software and community better.  Organizing SaaS-providing businesses as platform cooperatives will put people in control of software that affects our lives, which, in an age of flying killer robots, may be almost as important as the health and happiness of the Drupal community.

Come hear and talk about how Drupal LibreSaaS – like [Open Social](https://www.getopensocial.com/), [Roomify](https://roomify.us/), [RoundEarth](https://www.roundearth.io/), [Open Church](http://openchurchsite.com/), [Drutopia](https://drutopia.org/platform), and the platform you start – can save us all!



--- Cut to 1500


Drupal is better than ever, but whether it is more successful is questionable. A pincer threatens Drupal. One side, Drupal's own power and complexity, discourages new users and contributors. The other, proprietary platforms, increasingly squeeze out custom web development through sheer economies of scale. Retreating into Drupal's new fortress, the enterprise, leaves many of us on the outside— and it doesn't escape the pincer, which will continue until there's nowhere left to hide.

Adding a new economic model, that of software as a service, can make Drupal the best choice for quick starts and for nimble organizations. This, in turn, can drive contributions and innovation. Instead of waiting for proprietary SaaS companies to slowly add features and come eat our lunch, we can swoop in and eat theirs. At the same time, a well-designed Drupal SaaS provides many more people with the traditional entryway to web development and to Drupal.

Adhering to the principles of Free/Libre Software with our platforms will make the Drupal software and community better.  Organizing SaaS-providing businesses as platform cooperatives will put people in control of software that affects our lives, which, in an age of flying killer robots, may be almost as important as the health and happiness of the Drupal community.

Come hear and talk about how Drupal LibreSaaS (Open Social, FarmOS, Drutopia, and the platform you start) can save us all!




This session is for
people who believe Drupal, in all its complexity, has untapped potential for changing lives outside the "enterprise".



***

Learning Objectives & Outcomes: 

* What leads to long-term success in a software project?
* What Libre SaaS efforts exist in Drupal?
* Identify some areas where making the leap from services to product may make sense for you, your sector, and your business.


***

### Prerequsites:

* Any level of experience with Drupal.
* A desire to take more control of your life and work.

### Speaker credentials

<p>This presentation will build on iterations of Shaping Drupal and Future is Here talks given over the past four years at camps and summits, including <a href="http://2012.tcdrupal.org/sessions/shaping-drupals-future">Shaping Drupal's Future</a> at Twin Cities Drupal Camp, and Future is Here at NYCcamp, <a href="http://design4drupal.org/design-4-drupal-2015/sessions/future-here-whats-drupal-got-do-it">Design 4 Drupal Boston 2015</a> (<a href="https://www.youtube.com/watch?v=RdJeOrEm6q4">video</a>) and <a href="https://nerdsummit.org/nerdsummit-2015/sessions/future-here-whats-drupal-got-do-it">Nerd Summit 2015</a> (<a href="http://www.youtube.com/watch?v=G_etsXa2YNk">video</a>), and under the same title at <a href="http://2017.tcdrupal.org/session/saas-how-drupal-service-can-save-us-all">Twin Cities DrupalCamp 2017 (video)</a>.</p>

<p>Ben is passionate about using technology to connect people and to help them gain control over their lives and projects. A Drupal developer for more than 10 years, he has presented on a wide range of topics from migration and module development to design and contributing to the Drupal community. He has extensive speaking experience in and outside the Drupal world, and has presented at several DrupalCons, including:</p>

<ul>
	<li>DrupalCon Boston</li>
	<li>DrupalCon Washington, DC</li>
	<li>DrupalCon Paris</li>
	<li>DrupalCon Chicago</li>
	<li>DrupalCon Denver</li>
	<li>DrupalCon Portland (training)</li>
</ul>


## Twitter Teaser / Pull Quote

Instead of waiting for proprietary SaaS companies to slowly add features and come eat our lunch, we can swoop in and eat theirs.

## Metadata

Presentation topics: big ideas / business / Drupal community

business / big ideas / community

Track: Drupal Means Business

Experience Level: Intermediate

Timeslot: Saturday, 2:15pm - 3:15pm

Session Length: 60 minutes

### Submissions:

* **PRESENTED** http://2017.tcdrupal.org/session/saas-how-drupal-service-can-save-us-all (accepted; presented moderately well; recorded)
* https://2018.badcamp.net/session/how-drupal-service-can-save-our-livelihoods-and-our-lives
* https://www.drupalcampnj.org/node/103 (2019 camp)
* (not accepted) https://drupaldelphia.org/session/how-drupal-service-can-save-our-livelihoods-and-our-lives (2019)
* https://events.drupal.org/seattle2019/sessions/how-drupal-service-can-save-our-livelihoods-and-our-lives
* https://www.drupalgovcon.org/2019/program/sessions/how-drupal-service-can-save-our-livelihoods-and-our-lives
* https://2019.badcamp.org/session/how-drupal-service-can-save-our-livelihoods-and-our-lives
